/* 
* @Author: sd
* @Date:   2016-08-23 11:38:25
* @Last Modified by:   sd
* @Last Modified time: 2017-02-06 15:34:21
*/

$(document).ready(function(){
    // banner轮播
    $('.lubo').lubo({});

    $('.chaffle').chaffle().trigger('click');
    // 合作伙伴
    $('.index-partner').eq(0).slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 3,
        autoplay: false,
        autoplaySpeed: 3000,
    });

    // 媒体报告hover
    $('.index-media li').hover(function() {
        $(this).children('.media-tit').hide().siblings().show().parent().siblings().children('dl').hide().siblings().show();
    }, function() {
        // $(this).children('dl').hide().siblings().show()
    });


    // 右侧悬浮代码
    $(window).scroll(function (){
          scrollTop();
    });
    $('.top').click(function(){
        $('body,html').animate({
          scrollTop: 0
        },
        1000);
    })
    function scrollTop(){
      if($(window).scrollTop() >= 500){
        $('.fix-right').show();
      } else {
        $('.fix-right').hide();
      }
    };

});